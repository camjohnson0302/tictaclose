TicTacLose
==========

Tic Tac Toe that you will never win.


To run the specs, first create and migrate the test database.

   - rake db:create  RACK_ENV=test
   - rake db:migrate RACK_ENV=test




Todo
==========

- eliminate db calls where possible

- cascade destroy board, log winner, refactor out any space.last etc ?

- make_spaces spec doesn't test for correct indexes

- few more tests needed in general for player model

- fix up css, get rid of absolute position

- make it responsive, just for fun?

- refactor tests

- view test?

- refactor js...possibly just render from post route?

- composition test for player.take_turn 

- game.winning_sets Needs to be abtracted out. Also, better name?

- feature test?

- practice rewriting git history/writing good commit messages/logically grouping commits on feature branches

- remove hard coding if possible